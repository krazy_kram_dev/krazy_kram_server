const express = require('express');
const router = express.Router();
const User = require('../models/user');


router.get('/', (req, res) => {
    res.json({status: 'Register/Login Page'});
});

//Register Routes

router.post('/register', (req, res) => {
    // res.json({status : 'Register Post Link'});
    console.log(req.body);
    User.findOne({username : req.body.username}, function (err, user) {
        if(err) return res.json({error : err});
        if(user) return res.status(400).json({error : 'User already exists', loginSuccess : false});
        if(!user){
            const user = new User({
                firstname : req.body.firstname,
                lastname : req.body.lastname,
                username : req.body.username,
                email : req.body.username,
                password : req.body.password,
                country : req.body.country,
                number : req.body.number
            });
        
            user.save();
            console.log("See");
            res.json({ msg : 'User Registered' ,loginSuccess : true});
        }
    });
});

router.get('/register', (req, res) => {
    res.json({status: 'Register Page Route'});
});

//LOGIN Requests

router.post('/login', (req, res) => {
    console.log(req.body);
    // if(!User.findOne({username: req.body.username})) return res.json({err: 'User not found'}).status(400);
    User.findOne({ username: req.body.username}, (err, user) => {
        console.log(err, user);
        if(err) return res.json({error : err, loginSuccess : false});
        if(!user) return res.json({error: 'User not found', loginSuccess : false}); 
        user.comparePassword(req.body.password, (err, isMatch) => {
            if(err) return res.json({error :err, loginSuccess : false}).status(400);
            if(isMatch) return res.json({ msg : 'User found. Details Validated', loginSuccess : true, isAdmin : user.isAdmin});
            if(!isMatch) return res.json({ msg : 'Invalid Password' , loginSuccess : false});
        })
    });
    // console.log('Here Already');
});

router.get('/login', (req, res) => {
    res.json({status: 'Login Page Route'});
});

module.exports = router;
