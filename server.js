const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
// const mongoose = require('mongoose');
require('dotenv').config()

app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false }));
app.use(cookieParser());

// mongoose.connect(process.env.DATABASE_CONNECTION_STRING, { useNewUrlParser: true, autoIndex: false }).then(
//     () => {
//         console.log('Connected to DB');
//     },
//     (err) => {
//         console.log('Error connecting to DB');
//     }
// );


app.use('/registerlogin', require('./routes/registerLogin'));

app.get('/', (req, res)=> {
    console.log('Home Route');
    res.json({status: 'ok'});
});



app.listen(process.env.PORT, () => {
    console.log(`Server listening on port ${process.env.PORT}`);
})

//==============================
//         SOCKETS
//==============================

const webSocketsServerPort = 4000;
const webSocketServer = require('websocket').server;
const http = require('http');

const server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server,
    // not HTTP server
  }
);

server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port "
        + webSocketsServerPort);
  }
);

/**
* WebSocket server
*/
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket
    // request is just an enhanced HTTP request. For more info 
    // http://tools.ietf.org/html/rfc6455#page-6
    httpServer: server
  }
);

wsServer.on('request', function(request){
    var connection = request.accept(null, request.origin);
    

  //
  // New User has connected.  So let's record its socket
  //
  var user = new UserStruct(request.key, connection);

  //
  // Add the player to the list of all users
  //
    UsersList.push(user);
    console.log('Created new user, connected');
  //
  // We need to return the unique id of that user to the user itself
  //
    connection.sendUTF(JSON.stringify({action: 'connect', data: user.id}));
    console.log('Here');
    //
  // Listen to any message sent by that player
  //
  connection.on('message', function(data) {
      console.log("Message recvd");
    var message = JSON.parse(data.utf8Data);
    console.log(message);
    BroadcastToAllUsers(message);

    //Implement Switch Case...

});

connection.on('close', function(connection) {
    // We need to remove the corresponding user
    // TODO
  });
});


// -----------------------------------------------------------
// List of all Users
// -----------------------------------------------------------

var UsersList = [];

function UserStruct(id, connection){
    this.id = id;
    this.connection = connection;
    this.name = "";
    this.index = UsersList.length;
}

UserStruct.prototype = {
    getId: function(){
        return {name: this.name, id: this.id};
    },
};

// ---------------------------------------------------------
// Routine to broadcast the data to everyone
// ---------------------------------------------------------
function BroadcastToAllUsers(data){

    var message = JSON.stringify({
        'action': 'players_list',
        'data': 'hi'
    });

    UsersList.forEach(function(user){
        user.connection.sendUTF(JSON.stringify(data));
    });
}


  